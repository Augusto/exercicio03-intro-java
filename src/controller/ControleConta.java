package controller;

import java.util.ArrayList;

import model.Conta;

public class ControleConta {
	private ArrayList<Conta> listaContas;
	
	public ControleConta(){
		this.listaContas = new ArrayList<>();
	}
	
	public void adicionar(Conta novaConta){
		this.listaContas.add(novaConta);
	}
	
	public void excluir(Conta conta){
		this.listaContas.remove(conta);
	}
	
	public void depositar(Conta conta, float valorDeposito){
		valorDeposito = valorDeposito + conta.getSaldo();
		conta.setSaldo(valorDeposito);
	}
	
	public void retirar(Conta conta, float valorRetirada){
		valorRetirada = conta.getSaldo() - valorRetirada;
		conta.setSaldo(valorRetirada);
	}
	
	public float extrato(Conta conta){
		return conta.getSaldo();
	}
}
