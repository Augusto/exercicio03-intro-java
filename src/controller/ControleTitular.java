package controller;

import java.util.ArrayList;

import model.Titular;

public class ControleTitular {
	private ArrayList<Titular> listaTitulares;
	
	public ControleTitular(){
		this.listaTitulares = new ArrayList<>();
	}
	
	public void adicionar(Titular novoTitular){
		this.listaTitulares.add(novoTitular);
	}
	
	public void excluir(Titular titular){
		this.listaTitulares.remove(titular);
	}
	
}
