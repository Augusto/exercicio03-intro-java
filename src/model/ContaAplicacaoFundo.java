package model;

public class ContaAplicacaoFundo  extends Conta{
	private float rendimento;
	private float taxaAdministracao;
	
	public ContaAplicacaoFundo(Titular titular){
		super(titular);
	}
	
	public float getRendimento() {
		return rendimento;
	}
	public void setRendimento(float rendimento) {
		this.rendimento = rendimento;
	}
	public float getTaxaAdministracao() {
		return taxaAdministracao;
	}
	public void setTaxaAdministracao(float taxaAdministracao) {
		this.taxaAdministracao = taxaAdministracao;
	}
	
	
}
